#!/usr/bin/python3

# Script desenvolvido com o objetivo de acessar uma API, realizar o download
# de determinados arquivos e movê-los para um diretório onde serão processados pelo ?.
#

import os
import sys
import time
import shutil
import logging
import urllib3
import requests
import datetime
import unidecode
import smtplib
from lxml import etree
from email.mime.text import MIMEText
from urllib.parse import urlencode, quote_plus, quote

urllib3.disable_warnings()

atual 	= datetime.datetime.now()
hoje 	= atual.strftime('%Y%m')
mes 	= atual.strftime('%m')

# meses = ["03", "06", "09", "12"]

# mes  = "06"
# hoje = "202006"
# tipo = "xml"

# dirMove 	= 'move/'
dirLog 	= 'log/'
dirTmp 	= 'tmp/'

dir1 	= 'move/1/'
dir2 	= 'move/cadastro/'
dir3 	= 'move/valores/'


# Função responsável pelo envio de e-mail
def sendMail(text: str):

	# Assunto do e-mail
	SUBJECT = ''

	SMTP_SSL_HOST = ''
	SMTP_SSL_PORT = 

	USERNAME = ''
	PASSWORD = ''

	FROM 	 = ''
	TO 		 = ['']

	# Mensagem do e-mail
	MESSAGE 			= MIMEText(text)
	MESSAGE['subject'] 	= SUBJECT
	MESSAGE['from'] 	= FROM
	MESSAGE['to'] 		= ', '.join(TO)
	
	# Tentativa de envio de e-mail
	try:
		
		# Conexão com o servidor
		server = smtplib.SMTP_SSL(SMTP_SSL_HOST, SMTP_SSL_PORT)

		# Login no servidor
		server.login(USERNAME, PASSWORD)

		# Enviando e-mail
		server.sendmail(FROM, TO, MESSAGE.as_string())
	
		# Saindo do servidor
		server.quit()

		logger.info("E-mail enviado com sucesso para " + MESSAGE['to'])
		print("E-mail enviado com sucesso para " + MESSAGE['to'])

	except smtplib.SMTPAuthenticationError as e:
		print('Problemas na autententicacao do usuario "{}"' .format(USERNAME))
		logger.error('E-MAIL: Problemas na autententicacao do usuario "{}"' .format(USERNAME))
	except smtplib.SMTPNotSupportedError as e:
		print('Autententicacao nao suportada no servidor SMTP')
		logger.error('E-MAIL: Autententicacao nao suportada no servidor SMTP')
	except smtplib.SMTPException as e:
		print('Problemas ao enviar e-mail para "{}"' .format(MESSAGE['to']))
		logger.error('E-MAIL: Falha ao enviar e-mail para "{}"' .format(MESSAGE['to']))
	except Exception as e:
		# print(e)
		erro(e)


def moveFile(arquivo: str):
	
	# print(arquivo)

	msg = ''
	diretorio = ''

	if arquivo.startswith('1'):
		diretorio = dir1
	elif arquivo.startswith('2'):
		diretorio = dir2
	elif arquivo.startswith('3'):
		diretorio = dir3

	try:
		shutil.move(arquivo, diretorio+arquivo)
		logger.info('MOVE: Arquivo "{}" movido para o diretorio "{}"' .format(arquivo, diretorio))
		return 1
	except FileNotFoundError as e:
		msg = 'Arquivo "{}" ou diretorio "{}" nao existe' .format(arquivo, diretorio)
		print(msg)
		sendMail(msg)
		logger.error(msg)
		return 0
	except PermissionError as e:
		msg = 'MOVE: Permissao negada ao mover arquivo "{}" para o diretorio "{}"' .format(arquivo, diretorio)
		print(msg)
		sendMail(msg)
		logger.error(msg)
		return 0
	except Exception as e:
		print(str(e))
		erro(e)
		return 0

def createDir(diretorio: str):
	if(os.path.exists(diretorio)):
		pass
	else:
		logger.info('Criando diretório {}' .format(diretorio))
		os.mkdir(diretorio)


def checkFile(diretorio: str, arquivo: str) -> int:
	if os.path.isfile(diretorio+arquivo):
		logger.info('FLAG: Arquivo "{}" existe' .format(arquivo))
		return 1
	else:
		return 0

def deletaFlagAnterior(diretorio: str, arquivo: str, trimestral: bool):

	for _, _, arquivos in os.walk(diretorio):
	
		for arquivo in arquivos:
			
			file = arquivo.split('_')
			nome = file[0]
			data = file[1]

			if (nome == '3' and data != hoje and trimestral == True):
				os.remove(diretorio+arquivo)
				logger.info('DELETE: Arquivo "{}" excluido com sucesso' .format(arquivo))
			elif (nome != '3' and data != hoje and trimestral == False):
				os.remove(diretorio+arquivo)
				logger.info('DELETE: Arquivo "{}" excluido com sucesso' .format(arquivo))


def tryDelete():
	for arquivo in os.listdir('.'):
		
		file = arquivo.split('_')
		nome = file[0]

		if (nome == '1' or nome == '3' or nome == '2'):
			moveFile(arquivo)


def request(link: str) -> int:
	
	url 		= ''

	USER 		= ''
	PASSWD 		= ''

	arrayNome 	= link.split('(')
	arquivo   	= arrayNome[0] + '_' + hoje + '.' + tipo
	arquivoFlag	= arrayNome[0] + '_' + hoje + '_OK.txt'

	payload 	= {"$format": tipo, "@Data": hoje, "$top": "1"}
	# payload 	= {"$format": tipo, "@Data": hoje}
	headers 	= {'accept': 'application/json;odata.metadata=minimal'}
	payload 	= urlencode(payload, quote_via=quote, safe="'$@")

	try:
		response = requests.get(url+link, params=payload, verify=False, headers=headers, auth=(USER, PASSWD))

		print('GET: {}' .format(response.url))
		print('CODE: {}' .format(response.status_code))

		logger.info('GET: {}' .format(response.url))
		logger.info('STATUS: {}' .format(response.status_code))

		if response.status_code == 200:

			try:
				with open(arquivo, 'w', newline = '', encoding='UTF-8') as arq:
					# arq.write(unidecode.unidecode(response.text))
					arq.write(response.text)
				
				if tipo == 'xml':
					print('XML')
					doc 	= etree.parse(arquivo)
					root 	= doc.getroot()
					result 	= len(root.getchildren())
				elif tipo == 'json':
					print('JSON')
					result 	= len(response.json()['value'])
					
				print("Result: {}" .format(result))
				print('Arquivo {} finalizado' .format(link))

				if result > 0:

					move = moveFile(arquivo)

					try:
						# createDir(dirTmp)
						flag = open(dirTmp+arquivoFlag, 'w', newline = '')
						flag.close()
						logger.info('FLAG: Arquivo "{}" criado no diretorio "{}"' .format(arquivoFlag, dirTmp))
					except PermissionError as e:
						msg = 'FLAG: Permissao negada ao criar arquivo "{}" no diretorio "{}"' .format(arquivoFlag, dirTmp)
						print(msg)
						sendMail(str(msg))
						logger.error(msg)
					except Exception as e:
						erro(e)
				else:
					os.remove(arquivo)
					logger.info('Arquivo "{}" sem conteudo' .format(arquivo))
					logger.info('DELETE: Arquivo "{}" excluido com sucesso' .format(arquivo))

			except Exception as e:
				erro(e)
			return 1
		else:
			print(response.text)
			return 0

	except requests.exceptions.Timeout as e:
		sendMail('TIMEOUT: Requisição => {}' .format(url+link))
		logger.error('TIMEOUT: Requisição => {}' .format(url+link))

def erro(e):
	e = str(e)
	e = e.split(']')
	e = e[1].strip()
	print(e)
	logger.error(e)


# Criação do arquivo .log
DATA_EXEC = time.strftime("%Y")
os.makedirs(os.path.dirname(dirLog), exist_ok=True)
logging.basicConfig(level=logging.INFO,
					format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
					datefmt='%d/%m/%Y - %H:%M:%S',
					filename=dirLog+'bc_'+ DATA_EXEC +'.log',
					filemode='a')

logger = logging.getLogger(sys.argv[0])

# logger.info('Script iniciado')
	   
links = [
			'1(Data=@Data)',
			'2(Data=@Data)',
			'3(Data=@Data)'
		]

if tipo == 'xml':
	logger.info('Arquivos sendo baixado no formato XML')
elif tipo == 'json':
	logger.info('Arquivos sendo baixado no formato JSON')

tryDelete()

for link in links:

	arrayNome 	= link.split('(')

	arquivoFlag = arrayNome[0] + '_' + hoje + '_OK.txt'
	# arquivoFlag = arrayNome[0] + '_202002_OK.txt'

	# print(arquivoFlag)

	if (arrayNome[0] == '3' and (mes == '03' or mes == '06' or mes == '09' or mes == '12') and not checkFile(dirTmp, arquivoFlag)):

		logger.info('----------')
		logger.info('Arquivo ' .format(arrayNome[0]))

		request(link)

		deletaFlagAnterior(dirTmp, arquivoFlag, True)

	if (arrayNome[0] != '3' and not checkFile(dirTmp, arquivoFlag)):

		logger.info('----------')
		logger.info('Arquivo ' .format(arrayNome[0]))

		request(link)

		deletaFlagAnterior(dirTmp, arquivoFlag, False)

# 	print('\n')


logger.info('--------------------------------------------------------------------------------------------------------------')

# print(r.url)
# print(r.headers)
# print(r.content)
# print(r.text)
# print(r.status_code)

# auth=('', '')

